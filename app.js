const http = require('http')
const { PORT = 3000 } = process.env

const getRandomDinnerChoice = () => {
  const options = [
    'lasagne', 'bolognaise', 'tacos', 'shepards pie', 'curried sausages',
    'sloppy joes', 'rissoles', 'meatballs', 'corned beef', 'penne pesto',
    'cajan rub', 'lemon chicken', 'enchiladas', 'wings', 'pizza',
    'chicken parmigana', 'tikka massala',
  ];

  return options[Math.floor(Math.random() * options.length)];
}

const getThreeRandomDinnerChoices = () => {
  const choices = [];

  while (choices.length < 3) {
    let choice = getRandomDinnerChoice();

    while (choices.includes(choice)) {
      choice = getRandomDinnerChoice();
    }

    choices.push(choice);
  }

  return choices;
}

http.createServer((req, res) => {
  const dinners = getThreeRandomDinnerChoices();

  let dinnersItems = '';
  dinners.forEach(dinner => { dinnersItems = `${dinnersItems}<li>${dinner}</li>` });

  res.setHeader('Content-Type', 'text/html');
  res.write(`
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>What's For Dinner?</title>
    <style>
      html, body { margin: 0; padding: 0; }
      body { font: 2em sans-serif; }
      p { margin: 0; padding: 1em; background: #9C344C; color: #fff; }
      ul { margin: 0; padding: 0; }
      li { padding: 1em; border-bottom: 1px solid #9C344C; list-style-position: inside; }
    </style>
  </head>
  <body>
    <p>How about these ideas?</p>
    <ul>
      ${dinnersItems}
    </ul>
  </body>
</html>
  `);

  res.end();
}).listen(PORT);
